﻿using System;
using System.Windows;
using Microsoft.WindowsAPICodePack.Dialogs;
using PowerPoint = Microsoft.Office.Interop.PowerPoint;
using Core = Microsoft.Office.Core;
using System.IO;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace WpfApp1
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private String selectedFolderPath = "";     
        private String selectedThemePath = "";
        private String currentFileName = "";
        private Boolean currentFileStatus = false;
        private int conversionSuccess = 0;
        private int conversionFail = 0;
        private string[] files;                     //Contains the path for each file inside the selected folder
        

        public MainWindow()
        {
            InitializeComponent();            
        }

        public class Presentation
        {
            private String name;
            private String fileLink;
            private Boolean updated;

            public String filename
            {
                get { return name; }
                set { name = value; }
            }

            public String link
            {
                get { return fileLink; }
                set { fileLink = value; }
            }

            public Boolean isUpdated
            {
                get { return updated; }
                set { updated = value; }
            }

            
        }

        private void fillTable()
        {
            DataGrid.Items.Add(new Presentation() { filename = currentFileName, isUpdated = currentFileStatus, link= currentFileName });
        }

        private void SelectFolder_Click(object sender, RoutedEventArgs e)
        {
            SelectPath(true);
        }

        private void SelectTheme_Click(object sender, RoutedEventArgs e)
        {
            SelectPath(false);
        }

        /// <summary>
        /// Opens a window dialog to select a source folder or a theme file depending on the params
        /// </summary>
        /// <param name="isFolder">Boolean to determine if the dialog refers to the folder or the theme file</param>
        private void SelectPath(Boolean isFolder)
        {
            CommonOpenFileDialog dialog = new CommonOpenFileDialog();
            dialog.InitialDirectory = "C:\\Users";
            dialog.IsFolderPicker = isFolder;

            if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                String selectedPath = dialog.FileName;
                if (isFolder)
                {
                    selectedFolderPath = selectedPath;
                    FolderPath.Text = selectedFolderPath;
                }
                else
                {
                    selectedThemePath = selectedPath;
                    ThemePath.Text = selectedThemePath;
                }
            }
            else
            {
                MessageBox.Show("Error: Could not select folder");
            }
        }

        /// <summary>
        /// Async worker, manages all the logic to update the powerpoints
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            files = Directory.GetFiles(@selectedFolderPath, "*.ppt*", SearchOption.AllDirectories);

            int currentFile = 0; //File counter to update the progress bar
            var app = new PowerPoint.Application(); //instanciates the powerpoint object

            if(files.Length == 0)
            {
                MessageBox.Show("No presentation files found in selected folder");
                return;
            }

            foreach (string fileName in files)
            {
                currentFile++;
                currentFileName = fileName;
                //Create Presentation instance from the App object and open the file by path @fileName
                var presentation = app.Presentations;
                var file = presentation.Open(@fileName, Core.MsoTriState.msoTrue, Core.MsoTriState.msoTrue, Core.MsoTriState.msoFalse);

                try
                {
                    //Set aspect ratio to 16:9
                    file.PageSetup.SlideSize = PowerPoint.PpSlideSizeType.ppSlideSizeOnScreen16x9;
                }
                catch (Exception norb)
                {
                    String UnmodifiedFolderPath = @selectedFolderPath + "\\UNMODIFIED";
                    System.IO.Directory.CreateDirectory(UnmodifiedFolderPath);
                    System.IO.File.Move(@fileName, UnmodifiedFolderPath + "\\" + Path.GetFileName(fileName));
                    MessageBox.Show("File \n" + fileName + " was modified unsucessfully");
                }
                //Load new theme into the presentation
                file.Designs.Load(selectedThemePath);
                //Iterate through all the previous themes and delete them
                int totalTemplates = file.Designs.Count;
                for (int c=0; c < totalTemplates-1; c++)
                {
                    file.SlideMaster.Design.Delete();
                }
                
                //Overwrite file with the changes done and close the presentation instance
                file.SaveAs(@fileName);
                file.Close();

                //Report progress to main thread and update the UI progress bar and label with the file count
                currentFileStatus = true;
                conversionSuccess++;
                (sender as BackgroundWorker).ReportProgress(currentFile);
            }

            app.Quit(); //Closes the instance of powerpoint object
            MessageBox.Show("Conversion finished!");
        }

        /// <summary>
        /// Called everytime the worker finishes an iteration. Updates the progress bar and the label counting the files
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            ConversionProgress.Value = (e.ProgressPercentage*100)/files.Length;
            ProgressCounter.Content = "TOTAL PROGRESS: " + e.ProgressPercentage + "/" + files.Length;
            CorrectTranslation.Content = "SUCCESS: "+conversionSuccess;
            fillTable();
        }

        /// <summary>
        /// Start powerpoint conversion, instanciating a background worker 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StartConversion_Click(object sender, RoutedEventArgs e)
        {
            //Check that there is a folder and a path selected
            if(selectedFolderPath.Equals("") || selectedThemePath.Equals(""))
            {
                MessageBox.Show("You must select a folder and a theme before starting the conversion");
            }
            else if (! selectedThemePath.EndsWith("thmx"))
            {
                MessageBox.Show("Theme format must be .thmx");
            }
            else
            {
                //Initialize background worker
                BackgroundWorker worker = new BackgroundWorker();
                worker.WorkerReportsProgress = true;
                worker.DoWork += worker_DoWork;
                worker.ProgressChanged += worker_ProgressChanged;

                worker.RunWorkerAsync();
            }
        }
    }
}
